import sys
import os
from PySide import QtCore
from PySide import QtGui
p = os.path.abspath('..')
if p not in sys.path:
    sys.path.append(p)
from Nodes import *
from Commands import *
from AbstractGraph import *
from Settings import *
from Port import *
from BaseNode import *
from Edge import *
from Widget import *
from SyntaxHighlighter import Highlighter
from ConsoleInputWidget import ConsoleInput
import nodes_res_rc
