from PySide import QtGui, QtCore
from AbstractGraph import FLAG_SYMBOL


class Highlighter(QtGui.QSyntaxHighlighter):
    def __init__(self, parent=None, commandNameList= [], nodes_names = []):
        super(Highlighter, self).__init__(parent)

        comandPatterns = commandNameList
        commandNameFormat = QtGui.QTextCharFormat()
        commandNameFormat.setForeground(QtCore.Qt.cyan)
        commandNameFormat.setFontWeight(QtGui.QFont.Bold)


        self.highlightingRules = [(QtCore.QRegExp(pattern), commandNameFormat)
                for pattern in comandPatterns]

        singleLineCommentFormat = QtGui.QTextCharFormat()
        singleLineCommentFormat.setForeground(QtCore.Qt.darkYellow)
        self.highlightingRules.append((QtCore.QRegExp("//[^\n]*"),
                singleLineCommentFormat))

        flagFormat = QtGui.QTextCharFormat()
        flagFormat.setForeground(QtCore.Qt.darkCyan)
        # flagFormat.setFontWeight(QtGui.QFont.Bold)
        self.highlightingRules.append((QtCore.QRegExp("{0}\w+".format(FLAG_SYMBOL)),
                flagFormat))


        self.multiLineCommentFormat = QtGui.QTextCharFormat()
        self.multiLineCommentFormat.setForeground(QtCore.Qt.red)

        quotationFormat = QtGui.QTextCharFormat()
        quotationFormat.setForeground(QtCore.Qt.yellow)
        self.highlightingRules.append((QtCore.QRegExp("\'.*\'"),
                quotationFormat))

        functionFormat = QtGui.QTextCharFormat()
        functionFormat.setFontItalic(True)
        functionFormat.setForeground(QtCore.Qt.blue)
        self.highlightingRules.append((QtCore.QRegExp("\\b[A-Za-z0-9_]+(?=\\()"),
                functionFormat))

        self.commentStartExpression = QtCore.QRegExp("/\\*")
        self.commentEndExpression = QtCore.QRegExp("\\*/")

    def highlightBlock(self, text):
        for pattern, format in self.highlightingRules:
            expression = QtCore.QRegExp(pattern)
            index = expression.indexIn(text)
            while index >= 0:
                length = expression.matchedLength()
                self.setFormat(index, length, format)
                index = expression.indexIn(text, index + length)

        self.setCurrentBlockState(0)

        startIndex = 0
        if self.previousBlockState() != 1:
            startIndex = self.commentStartExpression.indexIn(text)

        while startIndex >= 0:
            endIndex = self.commentEndExpression.indexIn(text, startIndex)

            if endIndex == -1:
                self.setCurrentBlockState(1)
                commentLength = len(text) - startIndex
            else:
                commentLength = endIndex - startIndex + self.commentEndExpression.matchedLength()

            self.setFormat(startIndex, commentLength,
                    self.multiLineCommentFormat)
            startIndex = self.commentStartExpression.indexIn(text,
                    startIndex + commentLength);
